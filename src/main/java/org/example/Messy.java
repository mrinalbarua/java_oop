package org.example;

public class Messy extends Player {
    public Messy(){

    };
    public Messy(String name, int age, String country) {
        this.name = name;
        this.age = age;
        this.country = country;
    }

    @Override
    public void printName(){
        System.out.println(this.name);
    }
}
