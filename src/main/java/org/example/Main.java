package org.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        Player p0 = new Messy();
        Player p = new Messy("Messy", 32, "Argentina");
        System.out.println(p.name);
        p.printName();

        Messy m = new Messy("Messy", 32, "Argentina");
        m.printName();
//        A a0 = new B();
        A a = new B(1);
    }
}